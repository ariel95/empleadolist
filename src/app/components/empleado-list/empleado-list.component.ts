import { Component, OnInit } from '@angular/core';
import { Empleado } from 'src/app/models/empleado';

@Component({
  selector: 'app-empleado-list',
  templateUrl: './empleado-list.component.html',
  styleUrls: ['./empleado-list.component.css']
})
export class EmpleadoListComponent implements OnInit {

  listaEmpleados: Empleado[] = [
    {legajo: 1, nombre: 'Ariel', apellido: 'Paredes', sexo: 'Masculino', salario: 2500000},
    {legajo: 2, nombre: 'Marcos', apellido: 'Rivas', sexo: 'Masculino', salario: 2500000},
    {legajo: 3, nombre: 'Faustino', apellido: 'Villasboa', sexo: 'Masculino', salario: 2500000},
    {legajo: 4, nombre: 'Ana', apellido: 'Ayala', sexo: 'Femenino', salario: 2500000},
    {legajo: 5, nombre: 'Rosa', apellido: 'Cabrera', sexo: 'Femenino', salario: 2500000}
  ];

  radioSeleccionado = 'Todos';
  constructor() { }

  ngOnInit() {
  }

  obtenerTotalEmpleados(): number {
    return this.listaEmpleados.length;
  }
  obtenerTotalFem(): number {
    return this.listaEmpleados.filter(list => list.sexo === 'Femenino').length;
  }
  obtenerTotalMasc(): number {
    return this.listaEmpleados.filter(list => list.sexo === 'Masculino').length;
  }

  empleadoRadioButtonChange(radioSelect: string) {
    this.radioSeleccionado = radioSelect;
  }
}
